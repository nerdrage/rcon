package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/nerdrage/rcon"
)

var (
	rconHost string
	password string
)

func init() {
	flag.StringVar(&rconHost, "host", "", "server host:port")
}

func main() {
	flag.Parse()
	password = os.Getenv("URT_RCON_PASSWORD")

	if rconHost == "" {
		flag.PrintDefaults()
		os.Exit(2)
	}

	if password == "" {
		fmt.Fprint(os.Stderr, "WARNING: no rcon password provided. Set URT_RCON_PASSWORD environment variable.")
	}

	r, err := rcon.New(rconHost, password)
	if err != nil {
		log.Fatalln(err)
	}

	command := strings.Join(flag.Args(), " ")
	result, err := r.SendCmd(command)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(result)
}
