package rcon

import (
	"fmt"
	"net"
	"strings"
)

const (
	// MagicPrefix is sent with every command
	MagicPrefix = "\377\377\377\377"

	// CommandFormatString "<MagicPrefix>rcon <rcon password> <command>"
	CommandFormatString = "%srcon %s %s"

	// ResponseLength is the number of bytes we want to read from the response packet
	ResponseLength = 1024
)

// RCon implements methods for sending rcon commands
type RCon struct {
	addr     *net.UDPAddr
	password string
}

// New initializes RCon
func New(hostport, password string) (r *RCon, err error) {
	r = new(RCon)
	r.addr, err = net.ResolveUDPAddr("udp", hostport)
	if err != nil {
		return
	}
	r.password = password
	return
}

// PrepareCmd prepares command for sending
func (r *RCon) PrepareCmd(command string) []byte {
	return []byte(fmt.Sprintf(CommandFormatString, MagicPrefix, r.password, command))
}

// ReadResponse reads response to rcon command
func (r *RCon) ReadResponse(c *net.UDPConn) (result string, err error) {
	b := make([]byte, ResponseLength)
	var addr *net.UDPAddr
	for addr.String() != r.addr.String() {
		_, addr, err = c.ReadFromUDP(b)
		if err != nil {
			return
		}

		// TODO: log or something?
	}

	res := string(b)
	res = strings.TrimPrefix(res, MagicPrefix)
	res = strings.TrimPrefix(res, "print\n")
	result = res
	return
}

// SendCmd sends an rcon command
func (r *RCon) SendCmd(command string) (result string, err error) {
	cmd := r.PrepareCmd(command)

	c, err := net.DialUDP("udp", nil, r.addr)
	if err != nil {
		return
	}

	_, err = c.Write(cmd)
	if err != nil {
		return
	}

	result, err = r.ReadResponse(c)

	err = c.Close()
	if err != nil {
		return
	}
	return
}
